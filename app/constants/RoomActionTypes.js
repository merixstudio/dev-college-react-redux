export const ROOM_ADD = 'ROOM_ADD';
export const ROOM_UPDATE = 'ROOM_UPDATE';
export const ROOM_DELETE = 'ROOM_DELETE';
export const REQUEST_ROOMS = 'REQUEST_ROOMS';
export const RECEIVE_ROOMS = 'RECEIVE_ROOMS';
