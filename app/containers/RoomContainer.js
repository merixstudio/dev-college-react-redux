import React from 'react';
import { connect } from 'react-redux';

import MessageList from '../components/MessageList';
import MessageInput from '../components/MessageInput';

import Socket from '../socket';

class RoomContainer extends React.Component {
  sendMessage(text) {
    const { user } = this.props;

    Socket.emit('message', { text: text, from: user.name, roomId: user.currentRoom });
  }

  getRoomMessages() {
    const { messages, params } = this.props;
    console.log(this.props)
    return messages.items.filter((message) => {
      return message.roomId === params.roomId
    });
  }

  render() {
    const messages = this.getRoomMessages();

    return (
      <div style={{overflowY: 'auto', marginBottom: '58px'}}>
        <MessageList messages={messages} />
        <MessageInput onSend={(text) => this.sendMessage(text)} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    routing: state.routing,
    rooms: state.rooms,
    user: state.user,
    messages: state.messages
  }
}

export default connect(mapStateToProps)(RoomContainer);
