import React from 'react';

class LobbyContainer extends React.Component {
  render() {
    return (
      <div className="lobby">
        <h2 className="intro-text">Open menu on the left to join or create room</h2>
      </div>
    );
  }
}

export default LobbyContainer;
