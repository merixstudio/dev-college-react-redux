import React from 'react';
import { connect } from 'react-redux';

import AppBar from 'material-ui/lib/app-bar';
import IconButton from 'material-ui/lib/icon-button';
import LeftNav from 'material-ui/lib/left-nav';
import List from 'material-ui/lib/lists/list';
import ListItem from 'material-ui/lib/lists/list-item';
import Dialog from 'material-ui/lib/dialog';
import FlatButton from 'material-ui/lib/flat-button';
import TextField from 'material-ui/lib/text-field';
import NavigationClose from 'material-ui/lib/svg-icons/navigation/close';

// import IconMenu from 'material-ui/lib/menus/icon-menu';
// import MenuItem from 'material-ui/lib/menus/menu-item';
// import Colors from 'material-ui/lib/styles/colors';
// import MoreVertIcon from 'material-ui/lib/svg-icons/navigation/more-vert';

import { routeActions } from 'redux-simple-router'
import * as RoomActions from '../actions/RoomActions';
// import * as MessageActions from '../actions/MessageActions';
import * as UserActions from '../actions/UserActions';

class ChatContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sideNavOpen: false,
      modalOpen: false
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;

    dispatch(RoomActions.fetchRooms());
  }

  toggleNav() {
    this.setState({ sideNavOpen: !this.state.sideNavOpen });
  }

  handleModalOpen() {
    this.setState({ modalOpen: true });
  }

  handleModalClose() {
    this.setState({ modalOpen: false });
  }

  onRoomClick(room) {
    const { dispatch } = this.props;

    dispatch(routeActions.push('/room/' + room.id));
    dispatch(UserActions.joinRoom(room.id));

    this.setState({ sideNavOpen: false });
  }

  render() {
    const { rooms, children } = this.props;

    const actions = [
      <FlatButton
        label="Submit"
        primary={true}
        onTouchTap={() => {}} />,
      <FlatButton
        label="Cancel"
        secondary={true}
        onTouchTap={() => this.handleModalClose()} />
    ];

    const roomsItems = rooms.list.map((room) => {
      return (
        <ListItem primaryText={room.name} key={room.id} onTouchTap={() => this.onRoomClick(room)}/>
      )
    });

    return (
      <div className="container">
        <AppBar
          title="DevCollege React/Redux Chat Application"
          onLeftIconButtonTouchTap={() => this.toggleNav()} />
        {children}
        <LeftNav open={this.state.sideNavOpen} width={400} docked={false}>
          <AppBar title="Rooms List"
            iconElementLeft={<IconButton onTouchTap={() => this.toggleNav()}><NavigationClose /></IconButton>}
            iconElementRight={<FlatButton label="Add new room" onTouchTap={() => this.handleModalOpen()} />} />
          <List subheader="Rooms">
            {(rooms.list.length > 0) ? roomsItems : <ListItem disabled={true} primaryText="No rooms" />}
          </List>
        </LeftNav>
        <Dialog
          title="Name new room"
          actions={actions}
          modal={false}
          open={this.state.modalOpen}
          onRequestClose={() => this.handleModalClose()}>
          <TextField
            ref="roomName"
            hintText="Enter Room Name"
            floatingLabelText="Room name"
            defaultValue=""
            fullWidth={true} />
        </Dialog>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    routing: state.routing,
    rooms: state.rooms,
    user: state.user,
    messages: state.messages
  }
}

export default connect(mapStateToProps)(ChatContainer);
