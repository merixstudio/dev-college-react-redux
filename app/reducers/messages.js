import { MESSAGE_ADD, REQUEST_MESSAGES, RECEIVE_MESSAGES } from '../constants/MessageActionTypes';

const initialState = {
  isFetching: false,
  items: []
};

export function messages(state = initialState, action) {
  switch (action.type) {

    case MESSAGE_ADD: {
      return Object.assign({}, state, {
        items: [
          ...state.items,
          { id: action.id, text: action.text, roomId: action.roomId, from: action.from, createdAt: action.createdAt }
        ]
      });
    }

    case REQUEST_MESSAGES: {
      return Object.assign({}, state, {
        isFetching: true
      });
    }

    case RECEIVE_MESSAGES: {
      return Object.assign({}, state, {
        isFetching: false,
        items: action.messages
      });
    }

    default:
      return state;
  }
}
