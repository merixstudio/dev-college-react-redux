import { combineReducers } from 'redux';
import { routeReducer } from 'redux-simple-router';
import { rooms } from './rooms';
import { messages } from './messages';
import { user } from './user';

const rootReducer = combineReducers({
  routing: routeReducer,
  rooms: rooms,
  user: user,
  messages: messages
});

export default rootReducer;
