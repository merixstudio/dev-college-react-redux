import { REQUEST_ROOMS, RECEIVE_ROOMS, ROOM_ADD, ROOM_UPDATE, ROOM_DELETE } from '../constants/RoomActionTypes';

const initialState = {
  list: [],
  isFetching: false
};

export function rooms(state = initialState, action) {
  switch (action.type) {

    case ROOM_ADD: {
      return Object.assign({}, state, {
        list: [
          ...state.list,
          action.room
        ]
      });
    }

    case ROOM_UPDATE: {
      let roomIndex = null;

      state.list.forEach((room, index) => {
        if (room.id === action.room.id) {
          roomIndex = index;
        }
      });

      return Object.assign({}, state, {
        list: [
          ...state.list.slice(0, roomIndex),
          Object.assign({}, state.list[roomIndex], action.room),
          ...state.list.slice(roomIndex + 1)
        ]
      });
    }

    case ROOM_DELETE: {
      let roomIndex = null;
      let list = state.list.slice();

      list.forEach((room, index) => {
        if (room.id === action.room.id) {
          roomIndex = index;
        }
      });

      list.splice(roomIndex, 1);

      return Object.assign({}, state, {
        list: list
      });
    }

    case REQUEST_ROOMS: {
      return Object.assign({}, state, {
        isFetching: true
      });
    }

    case RECEIVE_ROOMS: {
      return Object.assign({}, state, {
        isFetching: false,
        list: action.rooms
      });
    }

    default:
      return state;
  }
}
