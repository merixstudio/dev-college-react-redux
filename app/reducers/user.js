import { USER_CHANGE_NAME, USER_JOIN_ROOM } from '../constants/UserActionTypes';

const initialState = {
  name: 'Guest',
  currentRoom: null
};

export function user(state = initialState, action) {
  switch (action.type) {
    case USER_JOIN_ROOM: {
      return Object.assign({}, state, {
        currentRoom: action.id
      });
    }

    case USER_CHANGE_NAME: {
      return Object.assign({}, state, {
        name: action.name
      });
    }

    default:
      return state;
  }
}
