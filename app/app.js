import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { connect, Provider } from 'react-redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { syncHistory } from 'redux-simple-router';
import thunkMiddleware from 'redux-thunk';
import Socket from './socket';

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

// Import reducer functions
import reducers from './reducers';

// Import styles
import '../public/styles/main.scss';

// Containers
import ChatContainer from './containers/ChatContainer';
import LobbyContainer from './containers/LobbyContainer';
import RoomContainer from './containers/RoomContainer';

// Actions
import * as RoomActions from './actions/RoomActions';
import * as MessageActions from './actions/MessageActions';

const reduxRouterMiddleware = syncHistory(browserHistory);
const createStoreWithMiddleware = applyMiddleware(thunkMiddleware, reduxRouterMiddleware)(createStore);
const store = createStoreWithMiddleware(reducers);

Socket.on('rooms:added', (data) => {
  store.dispatch(RoomActions.addRoom(data.new_val));
});

Socket.on('rooms:updated', (data) => {
  store.dispatch(RoomActions.updateRoom(data.new_val));
});

Socket.on('rooms:deleted', (data) => {
  store.dispatch(RoomActions.deleteRoom(data.old_val));
});

Socket.on('message:added', (data) => {
  store.dispatch(MessageActions.newMessage(data.new_val));
});

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Router history={browserHistory}>
          <Route path="/" component={ChatContainer}>
            <IndexRoute component={LobbyContainer} />
            <Route path="/room/:roomId" component={RoomContainer} />
          </Route>
        </Router>
      </Provider>
    );
  }
}

function mapStateToProps(state) {
  return {
    routing: state.routing,
    rooms: state.rooms
  }
}

export default connect(mapStateToProps)(App);

ReactDOM.render(<App/>, document.querySelector('#app'));
