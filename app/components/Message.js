import React from 'react';

export default class Message extends React.Component {
  render() {
    const { text, time, from } = this.props;

    return (
      <div className="message">
        <p className="message-meta">
          <span className="message-from">{from}</span>
          <span className="message-time">{time}</span>
        </p>
        <p className="message-text">{text}</p>
      </div>
    );
  }
}
