import React from 'react';
import TextField from 'material-ui/lib/text-field';

export default class MessageInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: ''
    }
  }

  handleEnterKey() {
    const { onSend } = this.props;

    onSend(this.state.value);
    this.setState({
      value: ''
    });
  }

  handleChange(e) {
    this.setState({
      value: e.target.value
    });
  }

  render() {
    return (
      <div className="message-input">
        <TextField
          ref="input"
          hintText="Message"
          onEnterKeyDown={() => this.handleEnterKey()}
          onChange={(e) => this.handleChange(e)}
          fullWidth={true}
          value={this.state.value} />
      </div>
    );
  }
}
