import { MESSAGE_ADD, REQUEST_MESSAGES, RECEIVE_MESSAGES } from '../constants/MessageActionTypes';

export function newMessage(message) {
  return {
    type: MESSAGE_ADD,
    id: message.id,
    text: message.text,
    roomId: message.roomId,
    from: message.from,
    createdAt: message.createdAt
  }
}

export function fetchMessages() {
  return dispatch => {
    dispatch(requestMessages())
    return fetch('http://localhost:3000/api/0/messages')
      .then(req => req.json())
      .then(json => dispatch(receiveMessages(json)))
  }
}

function requestMessages() {
  return {
    type: REQUEST_MESSAGES
  }
}

function receiveMessages(messages) {
  return {
    type: RECEIVE_MESSAGES,
    messages: messages
  }
}
