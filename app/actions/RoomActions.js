import { REQUEST_ROOMS, RECEIVE_ROOMS, ROOM_ADD, ROOM_UPDATE, ROOM_DELETE } from '../constants/RoomActionTypes';

export function addRoom(room) {
  return {
    type: ROOM_ADD,
    room: room
  }
}

export function updateRoom(room) {
  return {
    type: ROOM_UPDATE,
    room: room
  }
}

export function deleteRoom(room) {
  return {
    type: ROOM_DELETE,
    room: room
  }
}

export function fetchRooms() {
  return dispatch => {
    dispatch(requestRooms())
    return fetch('http://localhost:3000/api/0/rooms')
      .then(req => req.json())
      .then(json => dispatch(receiveRooms(json)))
  }
}

function requestRooms() {
  return {
    type: REQUEST_ROOMS
  }
}

function receiveRooms(rooms) {
  return {
    type: RECEIVE_ROOMS,
    rooms: rooms
  }
}
