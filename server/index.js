import path from 'path';
import bodyParser from 'body-parser';
import express from 'express';
import http from 'http';
import socketIO from 'socket.io';

import * as db from './db';
import * as api from './api/index';

var app = express();
var httpServer = http.createServer(app);
var port = process.env.PORT || 3000;

var io = socketIO(httpServer);

app.set('views', path.join(__dirname, 'server', 'views'));
app.set('view engine', 'jade');

app.use(require('serve-static')(path.join(__dirname, 'dist')));
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.use(db.createConnection);

api.rooms.liveUpdates(io);
api.messages.liveUpdates(io);
api.messages.listenToMessages(io);

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.get('/api/0/rooms', api.rooms.getRooms);
app.post('/api/0/rooms', api.rooms.addRoom);
app.get('/api/0/messages', api.messages.getMessages);
// app.post('/api/0/events/:id', api.editEvent);
// app.delete('/api/0/events/:id', api.deleteEvent);

app.use(db.closeConnection);

httpServer.listen(port, () => {
  console.log('Server listening on 3000!');
});
