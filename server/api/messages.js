import r from 'rethinkdb';
import { connect } from '../db';

export function liveUpdates(io) {
  connect()
  .then(conn => {
    r.table('messages')
    .changes().run(conn, (err, cursor) => {
      cursor.each((err, change) => {
        if (!change.old_val && change.new_val) {
          io.emit('message:added', change);
        }
      });
    });
  });
}

export function listenToMessages(io) {
  io.on('connection', function (socket) {
    socket.on('message', function(data) {
      var message = data;

      message.createdAt = r.now();

      connect()
      .then(conn => {
        r.table('messages')
          .insert(message, { returnChanges: true })
          .run(conn).then((result) => {
            return Object.assign({}, message, {id: result.generated_keys[0]});
          })
          .then((message) => message)
          .catch((err) => {
            console.log(err);
          });
      });
    });
  });
}

export function getMessages(req, res) {
  r.table('messages')
    .orderBy({ index: 'createdAt' })
    .run(req._rdbConn).then((cursor) => {
      return cursor.toArray();
    })
    .then((messages) => res.json(messages))
    .catch((err) => {
      res.status(400);
      res.json({ error: err });
    });
}
