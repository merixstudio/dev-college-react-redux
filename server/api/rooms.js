import r from 'rethinkdb';
import { connect } from '../db';

export function liveUpdates(io) {
  console.log('Setting up listener...');
  connect()
  .then(conn => {
    r.table('rooms')
    .changes().run(conn, (err, cursor) => {
      console.log('Listening for changes...');
      cursor.each((err, change) => {
        console.log('Change detected', change);
        if (!change.old_val && change.new_val) {
          io.emit('rooms:added', change);
        } else if (change.old_val && change.new_val) {
          io.emit('rooms:updated', change);
        } else if (change.old_val && !change.new_val) {
          io.emit('rooms:deleted', change);
        }
      });
    });
  });
}

export function getRooms(req, res) {
  r.table('rooms')
    .orderBy({ index: 'createdAt' })
    .run(req._rdbConn).then((cursor) => {
      return cursor.toArray();
    })
    .then((rooms) => res.json(rooms))
    .catch((err) => {
      res.status(400);
      res.json({ error: err });
    });
}

export function addRoom(req, res) {
  var room = req.body;
  console.log(req.rawBody);
  room.createdAt = r.now();

  r.table('rooms')
    .insert(room, { returnChanges: true })
    .run(req._rdbConn).then((result) => {
      return Object.assign({}, room, {id: result.generated_keys[0]});
    })
    .then((room) => res.json(room))
    .catch((err) => {
      res.status(400);
      res.json({ error: err });
    });
}
