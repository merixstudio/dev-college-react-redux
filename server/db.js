import r from 'rethinkdb';

export function connect() {
  return r.connect({
    host: '130.255.190.208',
    port: 12701,
    authKey: '',
    db: 'devcollege'
  });
}

export function createConnection(req, res, next) {
  connect().then(function(conn) {
    // Save the connection in `req`
    req._rdbConn = conn;
    // Pass the current request to the next middleware
    next();
  }).error(handleError(res));
}

function handleError(res) {
  return function(error) {
    res.send(500, {error: error.message});
  }
}

export function closeConnection(req, res, next) {
  req._rdbConn.close();
  next();
}
